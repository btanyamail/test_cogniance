# README #

### What is this repository for? ###

* Quick summary
This is a python code for testing of restful API - QA candidates. 
Using this code you can verify that data is added to server and verify correctness
 of returned response code for POST, GET and DELETE requests.

### How do I get set up? ###
1-CONTENTS OF THE PACKAGE
Python task:
test.py - source code

Datamining task:
script1.sh - script 
result.log - output of the script
datamining.log - source log

README	- this file

2-SYSTEM REQUIREMENTS
To run this code in Python IDLE the following libraries needed to be imported with the following commands:
import urllib2
import json
import unittest
import requests

3-HOW TO RUN A CODE
1. Open the test.py with Python IDLE
2. Click Run module from the menu to run the test

### Additional notes ###
There was an error in API with selecting a candidate by id. Sending a get/delete request with any id returns the
 candidate with id=90, which is actually the first record of the 'candidates' object.

### Who do I talk to? ###

* Tatiana Scherbakova