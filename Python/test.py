import urllib2
import json
import unittest
import requests

class TestServer(unittest.TestCase):
    def setUp(self):
        response = requests.post("http://qainterview.cogniance.com/candidates", json={"name":"Tanya","position":"QA engineer"})
        data = response.json()
        self.candidateId = data.get('candidate').get('id')
        print self.candidateId

    def test_getDataTest(self):
        url = 'http://qainterview.cogniance.com/candidates/'+str(self.candidateId)
        response = requests.get(url)
        print response.json()
        print url
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json().get('candidate').get('name'),'Tanya')
        
    def test_deleteDataTest(self):
        url = 'http://qainterview.cogniance.com/candidates/'+str(self.candidateId)
        response = requests.delete(url)
        print response.json()
        print url
        self.assertEqual(response.status_code, 200)

    def test_getAllDataTest(self):
        url = 'http://qainterview.cogniance.com/candidates'
        response = requests.get(url)
        print response.json()
        print url
        self.assertEqual(response.status_code, 200)
        
        
                
if __name__ == '__main__':
    unittest.main(verbosity=2)

